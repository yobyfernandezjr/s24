// alert("hi")

/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
const n = 2;
const getCube = n ** 3;
console.log(`The cube of ${n} is ${getCube}.`);


/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


// Code here:
// console.log("");

let address = ["#2", "St. John street", "San Isidro", "Paranaque City"]
let [houseNumber, street, barangay, city] = address;
console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${city}.`);

/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
// Code here:
// console.log("");
let animal = {
	habitat: "saltwater",
	name: "crocodile",
	weight: 1075,
	length: "20 ft 3 in"
};
let {habitat, name, weight, length} = animal;
console.log(`Lolong was a ${habitat} ${name}. He weighed at ${weight} kgs with a measurement of ${length}.`)


/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

// Code here:

// console.log("");
let numbers = [1,2,3,4,5,15];
numbers.forEach((number) => {
	console.log(`${number}`);
});

/*
Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

// Code here:

// console.log("");
class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};
let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);