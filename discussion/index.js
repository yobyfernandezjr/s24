

// ES6 Updates:

// Exponent Operator
const firstNum = Math.pow(8,2);
console.log(firstNum);

const secondNum = 8 ** 2;
console.log(secondNum);

// 5 raise to the power of 5
const thirdNum = 5**5; 
console.log(thirdNum);

/*
	Allows us to write strings without using the concatenation operator (+);
*/
let name = "George";


//Concatenation / Pre-template literal
// Using single quote (' ')
let message = 'Hello ' + name + ' Welcome to Zuitt Coding Bootcamp!';
console.log("Message without template literal: " + message);

console.log("");

// Strings using template literal
// Uses the backticks(` `)
message = `Hello ${name}. Welcome to Zuitt Coding Bootcamp!`;
console.log(`Message with template literal: ${message}`);

let anotherMessage = `
${name} attended a math competition. 
He won it by solving the problem 8**2 with the solution of ${secondNum}`;
console.log(anotherMessage);

anotherMessage = "\n" + name + "attended a math competition. \nHe won it by solving the problem 8**2 with the solution of " + secondNum + ". \n";
console.log(anotherMessage);

// Operation inside template literal
const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is: ${principal*interestRate}`);

// Array Destructing
/*
	Allows to unpack elements in an array into distinct variables. Allows us to name the array elements with variables instead of index number.

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

// Array Destructing
console.log("");
let [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log("");
console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// Object destructuring
/*
	Allows to unpack properties of objects into distinct variable. Shortens the syntax from accessing properties from objects.

	Syntax:
		let/const {propertyName, propertyName, propertyName} = object
*/

const person = {
	firstName1: "Jane",
	middleName1: "Dela",
	familyName: "Cruz",
};
// pre-object destructuring
console.log(person.firstName1);
console.log(person.middleName1);
console.log(person.familyName);

function getFullName(firstName1, middleName1, familyName) {
	console.log(`${firstName1} ${middleName1} ${familyName}`);
};

getFullName(person.firstName1, person.middleName1, person.familyName);

// Using Object Destructuring
const {middleName1, familyName, firstName1} = person;
console.log(firstName1);
console.log(middleName1);
console.log(familyName);

function getFullName(firstName1, middleName1, familyName) {
	console.log(`${firstName1} ${middleName1} ${familyName}`)
};

getFullName(firstName1, middleName1, familyName);

console.log("");
// Mini Activity:

	/*Item 1.)
		- Create a variable employees with a value of an array containing names of employees
		- Destructure the array and print out a message with the names of employees using Template Literals.*/


	// Code here:


	/*Item 2.)
		- Create a variable pet with a value of an object data type with different details of your pet as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
let employeesName = ["Aida", "Lorna", "Fe"];
let [employee1, employee2, employee3] = employeesName;
console.log(employee1);
console.log(employee2);
console.log(employee3);
console.log(`Employee names are ${employee1}, ${employee2}, ${employee3}.`);

console.log("");
let pet = {
	petName: "Aso",
	petGender: "Lalake",
	petColor: "brown"
}
let {petName, petGender, petColor} = pet;
console.log(petName);
console.log(petGender);
console.log(petColor);

function getPet(petName, petGender, petColor) {
	console.log(`My pet is ${petName}, ${petGender}, and ${petColor}.`)
}
getPet(petName, petGender, petColor);

 // PRE-ARROW FUNCTION and ARRO Function
 // Pre-Arrow Function
 /*
	Syntax:
		function functionName(paramA, paramB) {
			statement // console.log // return;
		}
 */
console.log("");
 function printFullName (firstName, middleInitial, lastName) {
 	console.log(firstName + " " + middleInitial + " " + lastName);
 };
 printFullName("Rupert", "B.", "Ramos");

 // Arrow function
/*
	Syntax:
		let/const variableName = (paramA, paramB) => {
			statement // console.log // return;
		}
*/ 	

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName1} ${middleInitial} ${lastName}`);
}

printFullName("John Robert", "A.", "Dela Vega")

const students = ["Yoby", "Emman", "Ronel"];
// Function with loop
// Pre-arrow function
students.forEach(function(student) {
	console.log(student + " is a student.");
});

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

function add(x,y) {
	return x+y;
};
let total = add(12, 15);
console.log(total);

// const addition = (x, y) => x + y;

// Use return keyword when curly braces is present
const addition = (x,y) => {
	return x+y;
}

let total2 = addition(12, 15);
console.log(total2);

// Default Function argument
const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());
console.log(greet("Archie"))

// Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using class as blueprint

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2019);
console.log(myNewCar);

